using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;

/// <summary>
/// handles UFO destruction and gives score to player
/// </summary>
[UpdateInGroup(typeof(InitializationSystemGroup))]
public class DestroyDeadUFOsSystem : SystemBase
{
    private EndInitializationEntityCommandBufferSystem commandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        commandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
    }
    protected override void OnUpdate()
    {
        EntityCommandBuffer entityCommandBuffer = commandBufferSystem.CreateCommandBuffer();

        Entities.WithAll<IsDeadTag>().ForEach((Entity entity, in UFOData ufo, in Translation trans) =>
        {
            entityCommandBuffer.DestroyEntity(entity);
            VFXManager.Instance.SpawnEffect(FXSpawnerData.EffectType.ExplosionGreen, trans.Value, quaternion.identity);

            GameManager.AddScore(GetScoreFromTier(ufo.Tier));
        }).WithoutBurst().Run();

        commandBufferSystem.AddJobHandleForProducer(Dependency);
    }

    int GetScoreFromTier(int tier)
    {
        switch (tier)
        {
            case 1:
                return 1000;
            case 2:
                return 200;
            default:
                return 0;
        }
    }
}
