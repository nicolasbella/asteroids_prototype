using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class SpaceshipBulletExtraAutoFiringSystem : SpaceshipBulletExtraFiringSystem
{
    protected override void OnUpdate()
    {
        if (Input.GetKey(FIRE_KEY))
        {
            var ecb = commandBufferSystem.CreateCommandBuffer();
            var bulletPrefab = PrefabsManager.Instance.BulletPlayer.Prefab;
            var elapsedTime = Time.ElapsedTime;

            quaternion angled = quaternion.AxisAngle(math.up(), math.radians(ANGLED_FIRED_EGREES));

            Entities.WithAll<PlayerSpaceshipData, HasExtraFiringData>().ForEach((ref Translation translation, in Rotation rotation, in HasAutofiringData autofiring) => {

                if (elapsedTime - autofiring.lastFiredTime >= AUTO_FIRING_DELAY)
                {
                    var newBullet = ecb.Instantiate(bulletPrefab);
                    ecb.SetComponent(newBullet, translation);
                    var rot = math.mul(math.mul(angled, rotation.Value), quaternion.RotateX(math.radians(90)));
                    ecb.SetComponent(newBullet, new Rotation { Value = rot });
                    ecb.AddComponent(newBullet, new MoveVelocityData { Velocity = math.mul(rot, math.up() * BULLET_SPEED) });

                    newBullet = ecb.Instantiate(bulletPrefab);
                    ecb.SetComponent(newBullet, translation);
                    rot = math.mul(math.mul(math.inverse(angled), rotation.Value), quaternion.RotateX(math.radians(90)));
                    ecb.SetComponent(newBullet, new Rotation { Value = rot });
                    ecb.AddComponent(newBullet, new MoveVelocityData { Velocity = math.mul(rot, math.up() * BULLET_SPEED) });
                }
            }).Schedule();

            commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
