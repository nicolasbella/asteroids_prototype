using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// system in charge of destroying the spaceship when is dead (has the IsDeadTag)
/// </summary>
public class DestroyDeadPlayerSpaceshipSystem : SystemBase
{
    public event Action SpaceshipDestroyed;

    protected override void OnUpdate()
    {
        Entities.WithAll<PlayerSpaceshipData, IsDeadTag>().ForEach((Entity e) =>
        {
            if (HasComponent<HasForcefieldData>(e)) EntityManager.DestroyEntity(GetComponent<HasForcefieldData>(e).ForcefieldEntity);
            EntityManager.DestroyEntity(e);
            SpaceshipDestroyed?.Invoke();

        }).WithoutBurst().WithStructuralChanges().Run();
    }
}
