using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// With this system we can spawn effects on the gameObjects world.
/// </summary>
[UpdateInGroup(typeof(InitializationSystemGroup))]
public class EffectsSpawnerSystem : SystemBase
{
    private EntityQuery query;
    protected override void OnUpdate()
    {        
        Entities.ForEach((ref FXSpawnerData fxSpawner) => {

            VFXManager.Instance.SpawnEffect(fxSpawner.Effect, fxSpawner.Position, fxSpawner.Rotation);
        }).WithoutBurst().WithStoreEntityQueryInField(ref query).Run();

        EntityManager.DestroyEntity(query);
    }
}
