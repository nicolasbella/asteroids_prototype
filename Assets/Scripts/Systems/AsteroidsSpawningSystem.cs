using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[AlwaysUpdateSystem]
public class AsteroidsSpawningSystem : SystemBase
{
    private EndInitializationEntityCommandBufferSystem commandBufferSystem;

    public const float MIN_SPEED = 1.2f;
    public const float MAX_SPEED = 3.5f;

    public const int INITIAL_ASTEROIDS = 4;
    public const int EXTRA_ASTEROIDS_PER_LEVEL = 2;

    protected override void OnCreate()
    {
        base.OnCreate();
        commandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
        GameManager.Instance.StateChanged += OnGameStateChanged;
    }

    protected override void OnUpdate()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            InstantiateAsteroids(); 
        }
    }

    private void OnGameStateChanged(GameManager.States s)
    {
        if (s == GameManager.States.Play)
        {
            InstantiateAsteroids();
        }
    }

    void InstantiateAsteroids()
    {
        var asteroids = EntityManager.Instantiate(PrefabsManager.Instance.AsteroidBig.Prefab, INITIAL_ASTEROIDS + (GameManager.Instance.CurrentLevel - 1) * EXTRA_ASTEROIDS_PER_LEVEL, Allocator.Temp);
        uint rngSeedSalt = (uint)UnityEngine.Random.Range(1, 999999);

        var worldX = MoveSystem.WorldLimitX;
        var worldY = MoveSystem.WorldLimitY;

        // I'm just using a normal for loop here instead of a job because I'm using the EntityManager anyway, creating a sync point,
        // since this is not a hot path and runs once every start of level, and it does structural changes anyway.
        for (int i = 0; i < asteroids.Length; i++)
        {
            var rng = Unity.Mathematics.Random.CreateFromIndex((uint)i + rngSeedSalt);

            float3 pos = rng.NextPointInRectangleBorder(worldX, worldY) * .9f;

            float3 dir = rng.Next3DDirectionOnXZPlane();
            float3 rotSpeed = math.radians(rng.NextFloat3(new float3(-90, -90, -90), new float3(90, 90, 90)));
            float speed = rng.NextFloat(MIN_SPEED, MAX_SPEED);

            EntityManager.SetComponentData(asteroids[i], new Translation { Value = pos });
            EntityManager.SetComponentData(asteroids[i], new MoveVelocityData { Velocity = dir * speed });
            EntityManager.SetComponentData(asteroids[i], new AsteroidData { Tier = 3 });
            EntityManager.SetComponentData(asteroids[i], new RotateData { RotationSpeedEuler = rotSpeed });
        }
    }
}
