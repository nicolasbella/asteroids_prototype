using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;

/// <summary>
/// Handles asteroids destruction and spawning of smaller ones
/// </summary>
[UpdateInGroup(typeof(InitializationSystemGroup))]
public class DestroyDeadAsteroidsSystem : SystemBase
{
    private EndInitializationEntityCommandBufferSystem commandBufferSystem;

    public const float TIER_SPEED_POWER = 1.4f;

    protected override void OnCreate()
    {
        base.OnCreate();
        commandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
    }
    protected override void OnUpdate()
    {
        EntityCommandBuffer entityCommandBuffer = commandBufferSystem.CreateCommandBuffer();

        Entity asteroidMediumPrefab = PrefabsManager.Instance.AsteroidMedium.Prefab;
        Entity asteroidSmallPrefab = PrefabsManager.Instance.AsteroidSmall.Prefab;

        Entities.WithAll<IsDeadTag>().ForEach((Entity entity, int entityInQueryIndex,in Translation translation, in AsteroidData asteroidData) =>
        {
            // if the asteroid is big or medium, we should spawn to smaller ones
            if (asteroidData.Tier > 1)
            {
                var rand = Random.CreateFromIndex((uint)entityInQueryIndex);
                var velMultiplier = math.pow(TIER_SPEED_POWER, 4 - asteroidData.Tier); // this will result in smaller asteroids having faster speeds
                var vel1 = rand.Next3DDirectionOnXZPlane() * (rand.NextFloat(AsteroidsSpawningSystem.MAX_SPEED - AsteroidsSpawningSystem.MIN_SPEED) + AsteroidsSpawningSystem.MIN_SPEED) * velMultiplier;
                var vel2 = rand.Next3DDirectionOnXZPlane() * (rand.NextFloat(AsteroidsSpawningSystem.MAX_SPEED - AsteroidsSpawningSystem.MIN_SPEED) + AsteroidsSpawningSystem.MIN_SPEED) * velMultiplier;
                var rot1 = math.radians(rand.NextFloat3(-90, 90));
                var rot2 = math.radians(rand.NextFloat3(-90, 90));
                // TODO: I tried before to instantiate the same entity and just change it's Scale, but the PhysicsCollider is static and doesn't change with the LocalToWorld scale component, you have to
                // mess around with BlobAssets, unsafe code, pointers, etc, so for the sake of simplicity I just made three different prefabs for each asteroid size.
                var prefab = asteroidData.Tier == 3 ? asteroidMediumPrefab : asteroidSmallPrefab;

                var newAsteroid1 = entityCommandBuffer.Instantiate(prefab);
                entityCommandBuffer.SetComponent(newAsteroid1, new MoveVelocityData { Velocity =  vel1});

                entityCommandBuffer.SetComponent(newAsteroid1, translation);
                entityCommandBuffer.SetComponent(newAsteroid1, new RotateData { RotationSpeedEuler = rot1 });

                var newAsteroid2 = entityCommandBuffer.Instantiate(prefab);
                entityCommandBuffer.SetComponent(newAsteroid2, new MoveVelocityData { Velocity = vel2 });
                entityCommandBuffer.SetComponent(newAsteroid2, translation);
                entityCommandBuffer.SetComponent(newAsteroid2, new RotateData { RotationSpeedEuler = rot2 });
            }
            entityCommandBuffer.DestroyEntity(entity);

            VFXManager.Instance.SpawnEffect(FXSpawnerData.EffectType.Explosion, translation.Value, quaternion.identity);

            // increase player score
            GameManager.AddScore(GetScoreFromTier((int)asteroidData.Tier));
        }).WithoutBurst().Run();

        commandBufferSystem.AddJobHandleForProducer(Dependency);
    }

    int GetScoreFromTier(int tier)
    {
        switch (tier)
        {
            case 1:
                return 100;
            case 2:
                return 50;
            case 3:
                return 20;
            default:
                return 0;
        }
    }
}
