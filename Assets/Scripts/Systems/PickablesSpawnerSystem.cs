using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

[UpdateInGroup(typeof(InitializationSystemGroup))]
public class PickablesSpawnerSystem : SystemBase
{
    private EndInitializationEntityCommandBufferSystem commandBufferSystem;

    const float MIN_DELAY_BETWEEN_SPAWNS = 8;
    const float MAX_DELAY_BETWEEN_SPAWNS = 18;
    float timeForNextSpawn;
    Random rng;

    protected override void OnCreate()
    {
        base.OnCreate();
        commandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
        rng = new Random((uint)UnityEngine.Random.Range(1, 99999999));
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        timeForNextSpawn = UnityEngine.Random.Range(MIN_DELAY_BETWEEN_SPAWNS, MAX_DELAY_BETWEEN_SPAWNS);
    }
    protected override void OnUpdate()
    {
        var ecb = commandBufferSystem.CreateCommandBuffer();
        timeForNextSpawn -= Time.DeltaTime;

        if (Input.GetKeyDown(KeyCode.P) || timeForNextSpawn <= 0)
        {
            var pickablePrefab = SelectPickablePrefab();

            var newPickable = ecb.Instantiate(pickablePrefab);


            // this should give us a random point in the rectangle border of the screen with uniform distribution (actually it covers the two positive sides like an L, but the world wrapping will make it look like the full rect)
            var pos = rng.NextPointInRectangleBorder(MoveSystem.WorldLimitX, MoveSystem.WorldLimitY);

            ecb.SetComponent(newPickable, new Translation { Value = pos });
            ecb.SetComponent(newPickable, new MoveVelocityData { Velocity = rng.Next3DDirectionOnXZPlane() * rng.NextFloat(1.5f, 3f) });

            commandBufferSystem.AddJobHandleForProducer(Dependency);

            // make the delay between spawns shorter so higher levels have more powerups to deal with the harder hazards
            timeForNextSpawn = UnityEngine.Random.Range(MIN_DELAY_BETWEEN_SPAWNS, MAX_DELAY_BETWEEN_SPAWNS) * math.max(.35f,(1f - ((float)(GameManager.Instance.CurrentLevel - 1f)) * 0.1f));
        }
    }

    Entity SelectPickablePrefab()
    {
        var types = System.Enum.GetValues(typeof(PickableData.PickableType));
        var selected = (PickableData.PickableType)types.GetValue(UnityEngine.Random.Range(0, types.Length));

        switch (selected)
        {
            case PickableData.PickableType.Forcefield:
                return PrefabsManager.Instance.PickableForcefield.Prefab;
            case PickableData.PickableType.Autofiring:
                return PrefabsManager.Instance.PickableAutofiring.Prefab;
            case PickableData.PickableType.Extrafiring:
                return PrefabsManager.Instance.PickableExtrafiring.Prefab;
            default:
                return PrefabsManager.Instance.PickableForcefield.Prefab;
        }
    }
}