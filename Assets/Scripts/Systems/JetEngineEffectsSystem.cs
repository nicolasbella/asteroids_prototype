using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
public class JetEngineEffectsSystem : SystemBase
{

    EntityQuery query;
    protected override void OnUpdate()
    {
        var vfx = VFXManager.Instance;
        float thrust = Input.GetAxis("Vertical");
        if (thrust > 0.5f && query.CalculateEntityCount() > 0)
        {
            if (!vfx.EngineThrustPS.isPlaying) vfx.EngineThrustPS.Play();
        }
        else if (!vfx.EngineThrustPS.isStopped) vfx.EngineThrustPS.Stop(); 


        Entities.WithAll<PlayerSpaceshipJetEngineTag>().WithStoreEntityQueryInField(ref query).ForEach((in LocalToWorld ltw) =>
        {
            vfx.EngineThrustTransform.position = ltw.Position;
            vfx.EngineThrustTransform.rotation = ltw.Rotation;
        }).WithoutBurst().Run();


    }
}
