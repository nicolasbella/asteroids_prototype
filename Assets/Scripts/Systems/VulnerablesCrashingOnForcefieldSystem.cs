using System.Runtime.CompilerServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;

[UpdateAfter(typeof(EndFramePhysicsSystem))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class VulnerablesCrashingOnForcefieldSystem : JobComponentSystem
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;


    protected override void OnCreate()
    {
        base.OnCreate();
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
    }

    [BurstCompile]
    struct VulnerablesCrashingOnForcefieldSystemJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<ForcefieldTag> allForcefields;

        public ComponentDataFromEntity<HealthData> allHealthData;

        public void Execute(TriggerEvent triggerEvent)
        {
            Entity A = triggerEvent.EntityA;
            Entity B = triggerEvent.EntityB;

            if (allHealthData.HasComponent(A) && allForcefields.HasComponent(B))
            {
                ProcessHit(A, allHealthData);
            }
            else if (allHealthData.HasComponent(B) && allForcefields.HasComponent(A))
            {
                ProcessHit(B, allHealthData);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ProcessHit(Entity crashedEntity, ComponentDataFromEntity<HealthData> allHealthData)
        {
            allHealthData[crashedEntity] = new HealthData { HealthRemaining = 0 };
        }
    }


    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
        var job = new VulnerablesCrashingOnForcefieldSystemJob();
        job.allForcefields = GetComponentDataFromEntity<ForcefieldTag>(true);
        job.allHealthData = GetComponentDataFromEntity<HealthData>(false);

        JobHandle jobHandle = job.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDependencies);

        jobHandle.Complete();
        return jobHandle;

    }
}
