using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// System in charge of depleting the duration of powerups and removing them when necessary.
/// </summary>
[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class PowerupsDepletionSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
    protected override void OnUpdate()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        float deltaTime = Time.DeltaTime;

        Entities.ForEach((Entity e, int entityInQueryIndex, ref HasAutofiringData autofiring) => {

            autofiring.RemainingTime -= deltaTime;
            if (autofiring.RemainingTime <= 0f)
            {
                ecb.RemoveComponent<HasAutofiringData>(entityInQueryIndex, e);
            }

        }).ScheduleParallel();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref HasExtraFiringData extrafiring) => {

            extrafiring.RemainingTime -= deltaTime;
            if (extrafiring.RemainingTime <= 0f)
            {
                ecb.RemoveComponent<HasExtraFiringData>(entityInQueryIndex, e);
            }

        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}