using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class SpaceshipControlSystem : SystemBase
{
    protected override void OnUpdate()
    {
        // Assign values to local variables captured in your job here, so that it has
        // everything it needs to do its work when it runs later.
        // For example,
        float deltaTime = Time.DeltaTime;
        float thrust = Mathf.Clamp01(Input.GetAxis("Vertical"));
        float turn = Input.GetAxis("Horizontal");

        float dampening = 0.1f;

        // This declares a new kind of job, which is a unit of work to do.
        // The job is declared as an Entities.ForEach with the target components as parameters,
        // meaning it will process all entities in the world that have both
        // Translation and Rotation components. Change it to process the component
        // types you want.
        
        
        
        Entities.ForEach((ref MoveVelocityData vel, ref Rotation rotation, in PlayerSpaceshipData data) => {
            // Implement the work to perform for each entity here.
            // You should only access data that is local or that is a
            // field on this job. Note that the 'rotation' parameter is
            // marked as 'in', which means it cannot be modified,
            // but allows this job to run in parallel with other jobs
            // that want to read Rotation component data.
            // For example,
            rotation.Value = math.mul(rotation.Value, quaternion.AxisAngle(math.up(), turn * data.TurnSpeed * deltaTime));
            vel.Velocity += math.forward(rotation.Value) * thrust * data.Acceleration * deltaTime;
            vel.Velocity *= 1f - dampening * deltaTime ;
        }).Schedule();
    }
}
