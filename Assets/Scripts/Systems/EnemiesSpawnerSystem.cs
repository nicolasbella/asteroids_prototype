using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

/// <summary>
/// handles instantiation of UFOs
/// </summary>
[UpdateInGroup(typeof(InitializationSystemGroup))]
public class EnemiesSpawnerSystem : SystemBase
{
    private EndInitializationEntityCommandBufferSystem commandBufferSystem;

    const float DELAY_BETWEEN_SPAWNS = 20;

    const float MIN_SPEED = 1.2f;
    const float MAX_SPEED = 4f;
    const float SMALL_UFO_SPEED_MUL = 1.3f;

    float timeForNextSpawn;
    Random rng;

    protected override void OnCreate()
    {
        base.OnCreate();
        commandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
        rng = new Random((uint)UnityEngine.Random.Range(1, 99999999));
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        timeForNextSpawn = DELAY_BETWEEN_SPAWNS;
    }
    protected override void OnUpdate()
    {
        var ecb = commandBufferSystem.CreateCommandBuffer();
        timeForNextSpawn -= Time.DeltaTime;

        if (Input.GetKeyDown(KeyCode.G) || timeForNextSpawn <= 0)
        {
            bool isSmallUFO = rng.NextBool();
            var ufoPrefab =  isSmallUFO ? PrefabsManager.Instance.UFOSmall.Prefab : PrefabsManager.Instance.UFOBig.Prefab;

            var newUFO = ecb.Instantiate(ufoPrefab);


            // this should give us a random point in the rectangle border of the screen with uniform distribution (actually it covers the two positive sides like an L, but the world wrapping will make it look like the full rect)
            var pos = rng.NextPointInRectangleBorder(MoveSystem.WorldLimitX, MoveSystem.WorldLimitY);

            // TODO: for some reason I'm not observing a uniform distribution in the UFOs directions, investigate that, maybe it's an emergent pattern in the underlying XORSHIFT but I don't think so.
            ecb.SetComponent(newUFO, new Translation { Value = pos });

            var speed = rng.NextFloat(MIN_SPEED, MAX_SPEED) * (isSmallUFO ? SMALL_UFO_SPEED_MUL : 1);
            ecb.SetComponent(newUFO, new MoveVelocityData { Velocity = rng.Next3DDirectionOnXZPlane() * speed });
            ecb.SetComponent(newUFO, new RandomNumberGeneratorData { rng = new Random(rng.NextUInt(1, uint.MaxValue - 1)) });
            if (isSmallUFO)
            {
                ecb.SetComponent(newUFO, new AimedBulletSpawnerData { Precision = math.clamp(GameManager.Instance.CurrentLevel / 5f, 0, 1) });
            }

            commandBufferSystem.AddJobHandleForProducer(Dependency);

            timeForNextSpawn = math.max(DELAY_BETWEEN_SPAWNS * .5f, DELAY_BETWEEN_SPAWNS - (GameManager.Instance.CurrentLevel - 1) * 2);
        }
    }
}