using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


/// <summary>
/// system in charge of spawning more bullets automatically whilst the fire key is held down
/// </summary>
[UpdateInGroup(typeof(InitializationSystemGroup)), UpdateAfter(typeof(SpaceshipBulletExtraAutoFiringSystem))]
public class SpaceshipBulletAutoFiringSystem : SpaceshipBulletFiringSystem
{
    double lastTimeSoundPlayed;

    EntityQuery query;
    protected override void OnUpdate()
    {
        if (Input.GetKey(FIRE_KEY))
        {
            var ecb = commandBufferSystem.CreateCommandBuffer();
            var bulletPrefab = PrefabsManager.Instance.BulletPlayer.Prefab;
            var elapsedTime = Time.ElapsedTime;

            Entities.WithAll<PlayerSpaceshipData>().WithStoreEntityQueryInField(ref query).ForEach((Entity e, ref Translation translation, ref HasAutofiringData autofiring, in Rotation rotation) =>
            {

                if (elapsedTime - autofiring.lastFiredTime >= AUTO_FIRING_DELAY)
                {
                    var newBullet = ecb.Instantiate(bulletPrefab);
                    ecb.SetComponent(newBullet, translation);

                    var rot = math.mul(rotation.Value, quaternion.RotateX(math.radians(90)));
                    ecb.SetComponent(newBullet, new Rotation { Value = rot });

                    ecb.AddComponent(newBullet, new MoveVelocityData { Velocity = math.mul(rot, math.up() * BULLET_SPEED) });
                    autofiring.lastFiredTime = elapsedTime;
                }

            }).Schedule();

            // dirty way of triggering the sounds without messing inside the Foreach ( the * 1.5f is to avoid triggering many overlapping sound FX)
            if (elapsedTime - lastTimeSoundPlayed >= AUTO_FIRING_DELAY * 1.5f && query.CalculateEntityCount() > 0)
            {
                SFXManager.PlaySound(SFXManager.Instance.ShotClip, 0.5f, UnityEngine.Random.Range(.9f, 1.1f));
                lastTimeSoundPlayed = elapsedTime;
            }

            commandBufferSystem.AddJobHandleForProducer(Dependency);

        }
    }
}
