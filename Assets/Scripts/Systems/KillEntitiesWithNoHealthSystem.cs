using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class KillEntitiesWithNoHealthSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
    protected override void OnUpdate()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        
        Entities.WithNone<IsDeadTag>().ForEach((Entity e, int entityInQueryIndex, in HealthData health) => {
            if (health.HealthRemaining <= 0)
            {
                ecb.AddComponent(entityInQueryIndex, e, new IsDeadTag());
            }

        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
