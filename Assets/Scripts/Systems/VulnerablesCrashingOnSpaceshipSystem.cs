using System.Runtime.CompilerServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;

[UpdateAfter(typeof(EndFramePhysicsSystem))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class VulnerablesCrashingOnSpaceshipSystem : JobComponentSystem
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;


    protected override void OnCreate()
    {
        base.OnCreate();
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
    }

    [BurstCompile]
    struct VulnerablesCrashingOnSpaceshipSystemJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<PlayerSpaceshipData> allSpaceships;

        public ComponentDataFromEntity<HealthData> allHealthData;

        public void Execute(TriggerEvent triggerEvent)
        {
            Entity A = triggerEvent.EntityA;
            Entity B = triggerEvent.EntityB;

            if (allHealthData.HasComponent(A) && allSpaceships.HasComponent(B))
            {
                ProcessAsteroidHit(B, A, allHealthData);
            }
            else if (allHealthData.HasComponent(B) && allSpaceships.HasComponent(A))
            {
                ProcessAsteroidHit(A, B, allHealthData);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ProcessAsteroidHit(Entity spaceship, Entity crashedEntity, ComponentDataFromEntity<HealthData> allHealthData)
        {
            HealthData h = allHealthData[spaceship];
            h.HealthRemaining = 0;
            allHealthData[spaceship] = h;
            allHealthData[crashedEntity] = h;
        }
    }


    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
        var job = new VulnerablesCrashingOnSpaceshipSystemJob();
        job.allSpaceships = GetComponentDataFromEntity<PlayerSpaceshipData>(true);
        job.allHealthData = GetComponentDataFromEntity<HealthData>(false);

        JobHandle jobHandle = job.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDependencies);

        jobHandle.Complete();
        return jobHandle;

    }
} 
