using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

/// <summary>
/// base system in charge of spawning the basic bullet when needed
/// </summary>
[UpdateInGroup(typeof(InitializationSystemGroup))]
public class SpaceshipBulletFiringSystem : SystemBase
{
    protected private EndInitializationEntityCommandBufferSystem commandBufferSystem;

    protected const float AUTO_FIRING_DELAY = 0.05f;
    protected const float BULLET_SPEED = 17f;

    protected const KeyCode FIRE_KEY = KeyCode.Space;


    protected override void OnCreate()
    {
        base.OnCreate();
        commandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
    }
    protected override void OnUpdate()
    {
        if (Input.GetKeyDown(FIRE_KEY))
        {
            var ecb = commandBufferSystem.CreateCommandBuffer();
            var bulletPrefab = PrefabsManager.Instance.BulletPlayer.Prefab;

            Entities.WithAll<PlayerSpaceshipData>().ForEach((ref Translation translation, in Rotation rotation) => {
                var newBullet = ecb.Instantiate(bulletPrefab);
                ecb.SetComponent(newBullet, translation);

                var rot = math.mul(rotation.Value, quaternion.RotateX(math.radians(90)));
                ecb.SetComponent(newBullet, new Rotation { Value = rot});

                ecb.AddComponent(newBullet, new MoveVelocityData { Velocity = math.mul(rot, math.up() * BULLET_SPEED) });
            }).Schedule();

            SFXManager.PlaySound(SFXManager.Instance.ShotClip, 0.5f, UnityEngine.Random.Range(.9f,1.1f));
            commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
