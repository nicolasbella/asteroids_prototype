using System.Runtime.CompilerServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;

[UpdateAfter(typeof(EndFramePhysicsSystem))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class PickPickablesSystem : JobComponentSystem
{
    const float FORCEFIELD_DURATION = 8f;
    const float AUTOFIRING_DURATION = 10f;
    const float EXTRAFIRING_DURATION = 10f;

    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    private EndSimulationEntityCommandBufferSystem commandBufferSystem;
    protected override void OnCreate()
    {
        base.OnCreate();
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();

        commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    [BurstCompile]
    struct PickPickablesSystemJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<PlayerSpaceshipData> allSpaceships;

        [ReadOnly] public ComponentDataFromEntity<PickableData> allPickables;

        [ReadOnly] public ComponentDataFromEntity<HasForcefieldData> hasForcefieldGroup;

        [ReadOnly] public ComponentDataFromEntity<LocalToWorld> localToWorldGroup;

        public EntityCommandBuffer entityCommandBuffer;

        public void Execute(TriggerEvent triggerEvent)
        {
            Entity A = triggerEvent.EntityA;
            Entity B = triggerEvent.EntityB;

            if (allPickables.HasComponent(A) && allSpaceships.HasComponent(B))
            {
                ProcessPickablePicking(B, A);
            }
            else if (allPickables.HasComponent(B) && allSpaceships.HasComponent(A))
            {
                ProcessPickablePicking(A, B);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ProcessPickablePicking(Entity spaceship, Entity pickable)
        {
            switch (allPickables[pickable].Type)
            {
                case PickableData.PickableType.Forcefield:

                    if (!hasForcefieldGroup.HasComponent(spaceship))
                    {
                        entityCommandBuffer.AddComponent<HasForcefieldData>(spaceship);
                        entityCommandBuffer.SetComponent(spaceship, new HasForcefieldData { RemainingTime = FORCEFIELD_DURATION });
                    }
                    else
                    {
                        var forcefield = hasForcefieldGroup[spaceship];
                        forcefield.RemainingTime = FORCEFIELD_DURATION;
                        entityCommandBuffer.SetComponent(spaceship, forcefield);
                    }

                    break;
                case PickableData.PickableType.Autofiring:

                    entityCommandBuffer.AddComponent<HasAutofiringData>(spaceship);
                    entityCommandBuffer.SetComponent(spaceship, new HasAutofiringData { RemainingTime = AUTOFIRING_DURATION });

                    break;
                case PickableData.PickableType.Extrafiring:

                    entityCommandBuffer.AddComponent<HasExtraFiringData>(spaceship);
                    entityCommandBuffer.SetComponent(spaceship, new HasExtraFiringData { RemainingTime = EXTRAFIRING_DURATION });

                    break;
                default:
                    break;
            }

            var fx = entityCommandBuffer.CreateEntity();
            entityCommandBuffer.AddComponent(fx, new FXSpawnerData { Effect = FXSpawnerData.EffectType.Picked, Position = localToWorldGroup[pickable].Position });
            entityCommandBuffer.DestroyEntity(pickable);
        }
    }


    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
        var job = new PickPickablesSystemJob();
        job.allSpaceships = GetComponentDataFromEntity<PlayerSpaceshipData>(true);
        job.allPickables = GetComponentDataFromEntity<PickableData>(true);
        job.localToWorldGroup = GetComponentDataFromEntity<LocalToWorld>(true);
        job.hasForcefieldGroup = GetComponentDataFromEntity<HasForcefieldData>(true);
        job.entityCommandBuffer = commandBufferSystem.CreateCommandBuffer();

        JobHandle jobHandle = job.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDependencies);

        //jobHandle.Complete();
        commandBufferSystem.AddJobHandleForProducer(jobHandle);
        return jobHandle;

    }
}
