using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

/// <summary>
/// system in charge of instantiating and destroying a Forcefield entity for entities with HasForcefield, and deplete the duration of them
/// </summary>
public class ForcefieldManagementSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var deltaTime = Time.DeltaTime;

        Entities.ForEach((Entity e, ref HasForcefieldData forcefield, ref PhysicsCollider col, in LocalToWorld ltw) => {

            // if the forcefield entity has been created for this forcefield, then reduce its lifetime
            if (EntityManager.Exists(forcefield.ForcefieldEntity))
            {
                forcefield.RemainingTime -= deltaTime;

                SetComponent(forcefield.ForcefieldEntity, new Translation { Value = ltw.Position });

                if (forcefield.RemainingTime <= 0)
                {
                    EntityManager.RemoveComponent<HasForcefieldData>(e);
                    EntityManager.DestroyEntity(forcefield.ForcefieldEntity);

                    var filter = col.Value.Value.Filter;
                    filter.CollidesWith = 0b_0001_1011; // restore collision to asteroids, bullets, UFOs, etc
                    col.Value.Value.Filter = filter;
                }
            }
            // otherwise create it and assign it (also change the collisionFilter of the spaceship so it doesn't collide with enemies or bullets
            else
            {
                var forcefieldEntity = EntityManager.Instantiate(PrefabsManager.Instance.Forceshield.Prefab);

                forcefield.ForcefieldEntity = forcefieldEntity;

                var filter = col.Value.Value.Filter;
                filter.CollidesWith &= 0b_0001_0000; // collide only with pickables
                col.Value.Value.Filter = filter;
            }

           

        }).WithoutBurst().WithStructuralChanges().Run();

        
        

    }
}
