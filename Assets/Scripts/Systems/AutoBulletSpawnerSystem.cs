using System.Runtime.CompilerServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// handles the bullet spawning for Automatic bullet spawners such as the UFOs
/// </summary>
[UpdateInGroup(typeof(InitializationSystemGroup))]
public class AutoBulletSpawnerSystem : SystemBase
{
    private EndInitializationEntityCommandBufferSystem commandBufferSystem;

    const float BULLET_VELOCITY = 12f;
    const float WORST_PRECISION_OFFSET = 3.3f;

    protected override void OnCreate()
    {
        base.OnCreate();
        commandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
    }


    protected override void OnUpdate()
    {
        var ecb = commandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        var bulletPrefab = PrefabsManager.Instance.BulletEnemy.Prefab;

        float deltaTime = Time.DeltaTime;

        // TODO: figure out how to use this method with inlining for better compiler optimization
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ProcessData(int entityInQueryIndex, ref AutoBulletSpawnerData bulletSpawner, Translation translation, EntityCommandBuffer.ParallelWriter ecb, float deltaTime, Entity bulletPrefab, quaternion directionRot)
        {
            bulletSpawner.TimeUntilNextSpawn -= deltaTime;

            if (bulletSpawner.TimeUntilNextSpawn <= 0)
            {
                var newBullet = ecb.Instantiate(entityInQueryIndex, bulletPrefab);
                ecb.SetComponent(entityInQueryIndex, newBullet, translation);

                var rot = math.mul(directionRot, quaternion.RotateX(math.radians(90)));
                ecb.SetComponent(entityInQueryIndex, newBullet, new Rotation { Value = rot });

                ecb.AddComponent(entityInQueryIndex, newBullet, new MoveVelocityData { Velocity = math.mul(rot, math.up() * BULLET_VELOCITY) });

                bulletSpawner.TimeUntilNextSpawn = 1f / bulletSpawner.RatePerSecond;

            }
        }

        // process random direction shooters
        Entities.WithNone<AimedBulletSpawnerData>().ForEach((Entity e, int entityInQueryIndex, ref AutoBulletSpawnerData bulletSpawner, ref RandomNumberGeneratorData rng, in Translation translation) => {

            var directionRot = quaternion.Euler(0, math.radians(rng.rng.NextFloat(360)), 0);

            ProcessData(entityInQueryIndex, ref bulletSpawner, translation, ecb, deltaTime, bulletPrefab, directionRot);

        }).ScheduleParallel();

        var playerQuery = EntityManager.CreateEntityQuery(typeof(PlayerSpaceshipData), typeof(LocalToWorld));

        if (playerQuery.CalculateEntityCount() > 0)
        {
            var localToWorldArray = playerQuery.ToComponentDataArray<LocalToWorld>(Allocator.Temp);
            var localToWorld = localToWorldArray[0];

            // process aimed direction shooters
            Entities.ForEach((Entity e, int entityInQueryIndex, ref AutoBulletSpawnerData bulletSpawner, ref RandomNumberGeneratorData rng, in AimedBulletSpawnerData aimed, in Translation translation) =>
            {
                // target  the spaceship
                var targetDir = math.normalize(localToWorld.Position - translation.Value);
                // but add some accuracy loss by using a normal vector as offset that gets smaller (thus the shooting gets more accurate) by the precision value
                var precisionOffset = math.mul(quaternion.AxisAngle(math.up(), math.radians(90) * (rng.rng.NextBool() ? 1 : -1)), targetDir) * WORST_PRECISION_OFFSET * (1 - aimed.Precision);
                targetDir = math.normalize(localToWorld.Position + precisionOffset - translation.Value);

                var aimedRot = quaternion.LookRotation(targetDir, math.up());

                ProcessData(entityInQueryIndex, ref bulletSpawner, translation, ecb, deltaTime, bulletPrefab, aimedRot);

            }).ScheduleParallel();

            localToWorldArray.Dispose();
        }

        playerQuery.Dispose();


        commandBufferSystem.AddJobHandleForProducer(Dependency);
        
    }

    

    
}
