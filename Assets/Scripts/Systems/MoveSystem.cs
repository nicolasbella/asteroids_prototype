using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class MoveSystem : SystemBase
{

    public const float WorldLimitX = 16f;
    public const float WorldLimitY = 9f;
    protected override void OnUpdate()
    {
        //return;
        float deltaTime = Time.DeltaTime;
        float3 worldLimitExtents = new float3(WorldLimitX * 2f, 1, WorldLimitY * 2f);
        float3 offset = new float3(WorldLimitX, 0.1f, WorldLimitY);


        Entities.ForEach((ref Translation translation, in MoveVelocityData vel) => {

            translation.Value += vel.Velocity * deltaTime;
            float3 x = translation.Value + offset;
            float3 y = worldLimitExtents;
            translation.Value = (x - y * math.floor(x / y)) - offset;
            translation.Value.y = 0;
        }).ScheduleParallel();
        
    }
}
