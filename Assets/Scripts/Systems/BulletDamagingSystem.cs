using System.Runtime.CompilerServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

/// <summary>
/// handles collisions (trigger events) between bullets and vulnerable entities (with HealthData)
/// </summary>
[UpdateAfter(typeof(EndFramePhysicsSystem))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class BulletDamagingSystem : JobComponentSystem
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    private EndSimulationEntityCommandBufferSystem commandBufferSystem;

    const int bulletDamage = 5;

    protected override void OnCreate()
    {
        base.OnCreate();
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    [BurstCompile]
    struct BulletDamagingSystemJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<BulletTag> allBullets;
        [ReadOnly] public ComponentDataFromEntity<LocalToWorld> localToWorldGroup;

        public ComponentDataFromEntity<HealthData> allHealthData;

        public EntityCommandBuffer entityCommandBuffer;

        public void Execute(TriggerEvent triggerEvent)
        {
            Entity A = triggerEvent.EntityA;
            Entity B = triggerEvent.EntityB;

            if (allBullets.HasComponent(A))
            {
                if (allHealthData.HasComponent(B))
                    ProcessBulletHit(A, B);
                else
                    entityCommandBuffer.DestroyEntity(A);

            }
            else if (allBullets.HasComponent(B))
            {
                if (allHealthData.HasComponent(A))
                    ProcessBulletHit(B, A);
                else
                    entityCommandBuffer.DestroyEntity(B);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ProcessBulletHit(Entity bullet, Entity damagedEntity)
        {
            entityCommandBuffer.DestroyEntity(bullet);
            HealthData h = allHealthData[damagedEntity];
            h.HealthRemaining -= bulletDamage;
            allHealthData[damagedEntity] = h;

            var e = entityCommandBuffer.CreateEntity();
            entityCommandBuffer.AddComponent(e, new FXSpawnerData { Effect = FXSpawnerData.EffectType.BulletHit, Position = localToWorldGroup[bullet].Position });
        }
    }


    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
        var job = new BulletDamagingSystemJob();
        job.allBullets = GetComponentDataFromEntity<BulletTag>(true);
        job.localToWorldGroup = GetComponentDataFromEntity<LocalToWorld>(true);
        job.allHealthData = GetComponentDataFromEntity<HealthData>(false);
        job.entityCommandBuffer = commandBufferSystem.CreateCommandBuffer();

        JobHandle jobHandle = job.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, inputDependencies);

        //jobHandle.Complete();
        commandBufferSystem.AddJobHandleForProducer(jobHandle);
        return jobHandle;
    }
} 
