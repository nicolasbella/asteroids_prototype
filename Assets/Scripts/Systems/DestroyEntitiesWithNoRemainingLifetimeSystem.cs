using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class DestroyEntitiesWithNoRemainingLifetimeSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
    protected override void OnUpdate()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        float deltaTime = Time.DeltaTime;

        Entities.WithNone<IsDeadTag>().ForEach((Entity e, int entityInQueryIndex, ref LifetimeData lifetime) => {

            lifetime.RemainingLifetime -= deltaTime;
            if (lifetime.RemainingLifetime <= 0f)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);
            }

        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
