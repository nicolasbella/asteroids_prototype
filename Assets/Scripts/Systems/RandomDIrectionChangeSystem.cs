using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class RandomDIrectionChangeSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        Entities.ForEach((ref RandomNumberGeneratorData rng, ref RandomDirectionChangeData dirChange, ref MoveVelocityData vel) => {

            dirChange.TimeUntilNextChange -= deltaTime;

            if (dirChange.TimeUntilNextChange <= 0)
            {
                float angle = rng.rng.NextFloat(dirChange.MinAngleChange, dirChange.MaxAngleChange) * (rng.rng.NextBool() ? 1 : -1);
                

                vel.Velocity = math.mul(quaternion.Euler(0, math.radians(angle), 0), vel.Velocity);

                dirChange.TimeUntilNextChange = rng.rng.NextFloat(dirChange.MinDelay, dirChange.MaxDelay);
            }

        }).ScheduleParallel();
    }
}
