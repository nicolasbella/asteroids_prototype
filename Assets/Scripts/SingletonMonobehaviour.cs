using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonMonobehaviour<T> : MonoBehaviour where T : SingletonMonobehaviour<T>
{
    static T _instance;
    public static T Instance
    {
        get
        {
            if (!isInitialized)
            {
                FindObjectOfType<T>().Initialize();
            }
            return _instance;
        }
    }


    static bool isInitialized;

    protected virtual void Awake()
    {
        if (!isInitialized)
        {
            Initialize();
        }
        else if (Instance != this)
        {
            Debug.LogWarning($"there are two instances of the singleton {typeof(T)}, deleting the newest one");
            Destroy(this);
        }
    }
    // Start is called before the first frame update
    protected virtual void Initialize()
    {
        _instance = this as T;
        isInitialized = true;
    }
}
