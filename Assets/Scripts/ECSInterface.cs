using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class ECSInterface : SingletonMonobehaviour<ECSInterface>
{
    public EntityManager Manager;
    public BlobAssetStore Store;

    // Start is called before the first frame update
    protected override void Initialize()
    {
        base.Initialize();

        Manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        Store = new BlobAssetStore();
    }

    public static Entity ConvertGameObjectHierarchy(GameObject root)
    {
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, Instance.Store);
        settings.ConversionFlags |= GameObjectConversionUtility.ConversionFlags.AssignName;
        Entity converted = GameObjectConversionUtility.ConvertGameObjectHierarchy(root, settings);
        return converted;
    }

    private void OnDestroy()
    {
        if (Store != null)
            Store.Dispose();
    }
}
