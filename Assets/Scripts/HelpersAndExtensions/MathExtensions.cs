using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.Mathematics;
using UnityEngine;

public static class MathExtensions
{

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 ToX0Y(this float2 xy)
    {
        return new float3(xy.x, 0, xy.y);
    }

    /// <summary>
    /// gives a random point in the rectangle border of defined size with uniform distribution (only in the positive sides like an L shape)
    /// </summary>
    /// <param name="rng"></param>
    /// <param name="rectHalfSizeX">half the x size of the desired rect (if the rect is 10 in size, this should be 5)</param>
    /// <param name="rectHalfSizeY">half the y size of the desired rect (if the rect is 10 in size, this should be 5)</param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 NextPointInRectangleBorder(this Unity.Mathematics.Random rng, float rectHalfSizeX, float rectHalfSizeY)
    {
        //var posInRectanglePerimeter = rng.NextFloat(0, MoveSystem.WorldLimitX + MoveSystem.WorldLimitY);
        //// this should give us a random point in the rectangle border of the screen with uniform distribution (actually it covers the two positive sides like an L, but the world wrapping will make it look like the full rect)
        //var posX = math.clamp(posInRectanglePerimeter, 0, MoveSystem.WorldLimitX) * 2f - MoveSystem.WorldLimitX;
        //var posY = (math.clamp(posInRectanglePerimeter, MoveSystem.WorldLimitX, MoveSystem.WorldLimitX + MoveSystem.WorldLimitY) - MoveSystem.WorldLimitX) * 2 - MoveSystem.WorldLimitY;

        var r1 = rng.NextFloat(rectHalfSizeX + rectHalfSizeY);
        var movesOnX = r1 <= rectHalfSizeX;



        return new float3((movesOnX ? r1 : rectHalfSizeX) * (rng.NextBool() ? 1 : -1), 0, (movesOnX ? rectHalfSizeY : (r1 - rectHalfSizeX)) * (rng.NextBool() ? 1 : -1));
    }

    /// <summary>
    /// method created as a workaround, since NextFloat2Direction() shows pseudo rng patterns and is not uniformely distributed.
    /// </summary>
    /// <param name="rng"></param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 Next3DDirectionOnXZPlane(this Unity.Mathematics.Random rng)
    {
        // TODO: workaround, if I don't multiply by 50 or some big number, some pseudorandom pattern emerges in which all entities spawn in clustered groups of directions instead of uniformly distributed
        // multiplying by a big number "hides" the pattern a bit (that exact same pattern is shown if we use the commented method NextFloat2DDirection(), so it is effectively useless
        return math.forward(quaternion.AxisAngle(math.up(), rng.NextFloat(0, 50 * 2 * math.PI)));
    }
}