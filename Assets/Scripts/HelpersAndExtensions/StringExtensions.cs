using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public static class StringExtensions
{
    public static string Repeat(this string s, int times)
    {
        if (string.IsNullOrEmpty(s) || times <= 0) return "";
        return new StringBuilder(s.Length * times).Insert(0, s, times).ToString();
    }
}
