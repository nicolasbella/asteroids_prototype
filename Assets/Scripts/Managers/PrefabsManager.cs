using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

/// <summary>
/// Holds the Prefabs for future instantiation and converts them into entity prefabs
/// </summary>
public class PrefabsManager : SingletonMonobehaviour<PrefabsManager>
{

    public EntityPrefab PlayerSpaceship;

    public EntityPrefab BulletEnemy;

    public EntityPrefab BulletPlayer;

    public EntityPrefab Forceshield;

    [Header("Pickables")]
    public EntityPrefab PickableForcefield;
    public EntityPrefab PickableAutofiring;
    public EntityPrefab PickableExtrafiring;

    [Header("Enemies")]
    public EntityPrefab UFOBig;
    public EntityPrefab UFOSmall;

    [Header("Asteroids")]
    public EntityPrefab AsteroidBig;
    public EntityPrefab AsteroidMedium;
    public EntityPrefab AsteroidSmall;


    [Space(16), Header("FX Prefabs")]
    public GameObject BulletHitFX;
    public GameObject ExplosionFX;
    public GameObject ExplosionGreenFX;
    public GameObject PickedFX;
    public GameObject HyperspaceFX;

    protected override void Initialize()
    {
        base.Initialize();

        UFOBig.Init();
        UFOSmall.Init();
        PlayerSpaceship.Init();
        BulletEnemy.Init();
        BulletPlayer.Init();
        PickableForcefield.Init();
        PickableAutofiring.Init();
        PickableExtrafiring.Init();
        Forceshield.Init();
        AsteroidBig.Init();
        AsteroidMedium.Init();
        AsteroidSmall.Init();
    }
}

[Serializable]
public class EntityPrefab
{
    [SerializeField]
    GameObject go;
    Entity _prefab;

    public Entity Prefab => _prefab;

    public void Init()
    {
        _prefab = ECSInterface.ConvertGameObjectHierarchy(go);
    }
}
