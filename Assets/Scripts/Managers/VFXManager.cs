using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Singleton in charge of spawning and pooling VFX
/// </summary>
public class VFXManager : SingletonMonobehaviour<VFXManager>
{
    [SerializeField]
    Material skyboxMat;

    [SerializeField]
    private ParticleSystem _engineThrustPS;
    public Transform EngineThrustTransform { get; private set; }
    public ParticleSystem EngineThrustPS => _engineThrustPS;

    protected override void Initialize()
    {
        base.Initialize();
        EngineThrustTransform = _engineThrustPS.transform;
    }

    private void Update()
    {
        skyboxMat.SetFloat("_Rotation", Time.time % 360f);
    }

    public void SpawnEffect(FXSpawnerData.EffectType effect, Vector3 pos, Quaternion rot)
    {
        switch (effect)
        {
            case FXSpawnerData.EffectType.Explosion:

                SimplePool.Spawn(PrefabsManager.Instance.ExplosionFX, pos, rot);
                break;
            case FXSpawnerData.EffectType.ExplosionGreen:

                SimplePool.Spawn(PrefabsManager.Instance.ExplosionGreenFX, pos, rot);
                break;
            case FXSpawnerData.EffectType.BulletHit:

                SimplePool.Spawn(PrefabsManager.Instance.BulletHitFX, pos, rot);
                break;
            case FXSpawnerData.EffectType.Picked:

                SimplePool.Spawn(PrefabsManager.Instance.PickedFX, pos, rot);
                break;
            case FXSpawnerData.EffectType.HyperSpace:

                SimplePool.Spawn(PrefabsManager.Instance.HyperspaceFX, pos, rot);
                break;
            default:
                break;
        }
    }
}
