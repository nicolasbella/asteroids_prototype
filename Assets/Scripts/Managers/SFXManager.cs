using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : SingletonMonobehaviour<SFXManager>
{
    [SerializeField] AudioSource source;

    public AudioClip ShotClip;
    public AudioClip HyperspaceClip;

    public static void PlaySound(AudioClip clip, float volume = 1f)
    {
        Instance.source.pitch = 1;
        Instance.source.PlayOneShot(clip, volume);
    }

    public static void PlaySound(AudioClip clip, float volume, float pitch)
    {
        Instance.source.pitch = pitch;
        Instance.source.PlayOneShot(clip, volume);
    }
}
