using MonsterLove.StateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UIElements;

public class GameManager : SingletonMonobehaviour<GameManager>
{
    #region static
    static int _totalScore;
    public static int TotalScore
    {
        get => _totalScore;
        private set
        {
            _totalScore = value;
            TotalScoreChanged?.Invoke();
        }
    }

    public static void AddScore(int amount)
    {
        int tenThousands = TotalScore / 10000;
        TotalScore += amount;

        // if we passed a 10000 barrier, give extra life
        if (tenThousands < TotalScore / 10000)
        {
            Instance.CurrentLives++;
        }
    }

    public static event Action TotalScoreChanged;

    #endregion static

    [SerializeField]
    int startingLives;
    int _currentLives;
    public int CurrentLives
    {
        get => _currentLives;
        private set
        {
            _currentLives = value;
            CurrentLivesChanged?.Invoke();
        }
    }

    int _currentLevel;
    public int CurrentLevel
    {
        get => _currentLevel;
        private set
        {
            _currentLevel = value;
            CurrentLevelChanged?.Invoke();
        }
    }

    int _currentHyperspaces;
    public int CurrentHyperspaces
    {
        get => _currentHyperspaces;
        private set
        {
            _currentHyperspaces = value;
            CurrentHyperspacesChanged?.Invoke();
        }
    }

    public event Action CurrentLivesChanged;
    public event Action CurrentLevelChanged;
    public event Action CurrentHyperspacesChanged;
    public event Action GameStarted;

    public enum States
    {
        Init,
        Play,
        WonLevel,
        GameOver,
    }

    StateMachine<States, Driver> fsm;

    public event Action<States> StateChanged;
    public States CurrentState => fsm.State;
    public class Driver
    {
        public StateEvent Update;
    }

    protected override void Initialize()
    {
        base.Initialize();

        fsm = new StateMachine<States, Driver>(this);

        fsm.Changed += FsmStateChanged;
        fsm.ChangeState(States.Init);

        // TODO: this is a workaround to add this systems, seems that inheritance doesn't play well with the autodiscovery and creation of systems so I have to manually add the parent class systems manually
        World defaultWorld = ECSInterface.Instance.Manager.World;
        InitializationSystemGroup systemGroup = defaultWorld.GetOrCreateSystem<InitializationSystemGroup>();
        systemGroup.AddSystemToUpdateList(defaultWorld.CreateSystem<SpaceshipBulletFiringSystem>());
        systemGroup.AddSystemToUpdateList(defaultWorld.CreateSystem<SpaceshipBulletExtraFiringSystem>());
        systemGroup.SortSystems();
    }

    private void FsmStateChanged(States s)
    {
        StateChanged?.Invoke(s);
    }

    private void Update()
    {
        fsm.Driver.Update.Invoke();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    #region Init State

    void Init_Enter()
    {
        Debug.Log("Ready To Play");
        TotalScore = 0;
    }

    void Init_Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            fsm.ChangeState(States.Play);
        }
    }

    void Init_Exit()
    {
        CleanExistingEntities();
    }

    void CleanExistingEntities()
    {
        var queryAsteroids = ECSInterface.Instance.Manager.CreateEntityQuery(typeof(AsteroidData));
        var queryUFOs = ECSInterface.Instance.Manager.CreateEntityQuery(typeof(UFOData));
        var arrayUFOs = queryUFOs.ToEntityArray(Unity.Collections.Allocator.Temp);
        var queryPickables = ECSInterface.Instance.Manager.CreateEntityQuery(typeof(PickableData));
        var arrayPickables = queryPickables.ToEntityArray(Unity.Collections.Allocator.Temp);

        ECSInterface.Instance.Manager.DestroyEntity(queryAsteroids);
        ECSInterface.Instance.Manager.DestroyEntity(arrayUFOs);
        ECSInterface.Instance.Manager.DestroyEntity(arrayPickables);

        queryAsteroids.Dispose();
        queryPickables.Dispose();
        queryUFOs.Dispose();
        arrayPickables.Dispose();
        arrayUFOs.Dispose();
    }

    #endregion Init State

    #region PLAY_STATE

    void Play_Enter()
    {
        // check if we are playing the first level, so we spawn the spaceship, or we are playing a subsequent level (level 0 means we came from Init state, Level > 0 means we won last level and we should respawn asteroids
        if (CurrentLevel == 0)
        {
            Debug.Log("Spawning Player");
            CurrentLives = startingLives;
            CurrentHyperspaces = 2;
            SpawnSpaceship();
            GameStarted?.Invoke();

        }

        CurrentLevel++;

        ECSInterface.Instance.Manager.World.GetOrCreateSystem<DestroyDeadPlayerSpaceshipSystem>().SpaceshipDestroyed += OnSpaceshipDestroyed;
    }

    void SpawnSpaceship()
    {
        ECSInterface.Instance.Manager.Instantiate(PrefabsManager.Instance.PlayerSpaceship.Prefab);
    }

    private void OnSpaceshipDestroyed()
    {
        CurrentLives--;
        if (CurrentLives > 0)
        {
            SpawnSpaceship();
        }
        else
        {
            fsm.ChangeState(States.GameOver);
        }
    }

    void Play_Update()
    {
        // Do hyperspace if requested
        if (Input.GetKeyDown(KeyCode.Tab) && CurrentHyperspaces > 0)
        {
            var query = ECSInterface.Instance.Manager.CreateEntityQuery(typeof(PlayerSpaceshipData));
            var array = query.ToEntityArray(Unity.Collections.Allocator.Temp);

            if (array.Length > 0)
            {
                // if the HasForcefieldData component is already there, we only want to edit its remaining life, if instead we use AddComponent and override it, the link to the Forcefield entity would be lost
                float3 newPos = new float3(UnityEngine.Random.Range(-MoveSystem.WorldLimitX, MoveSystem.WorldLimitX), 0, UnityEngine.Random.Range(-MoveSystem.WorldLimitY, MoveSystem.WorldLimitY));
                ECSInterface.Instance.Manager.SetComponentData(array[0], new Translation {Value = newPos});
                if (ECSInterface.Instance.Manager.HasComponent(array[0], typeof(HasForcefieldData)))
                {
                    var forcefield = ECSInterface.Instance.Manager.GetComponentData<HasForcefieldData>(array[0]);
                    forcefield.RemainingTime = 1f;
                    ECSInterface.Instance.Manager.SetComponentData(array[0], forcefield);
                }
                else
                {
                    ECSInterface.Instance.Manager.AddComponentData(array[0], new HasForcefieldData {RemainingTime = 1f });
                }
                VFXManager.Instance.SpawnEffect(FXSpawnerData.EffectType.HyperSpace, newPos, quaternion.identity);
                CurrentHyperspaces--;
            }
            query.Dispose();
            array.Dispose();
        }

        // Winning conditions
        var remainingAsteroids = ECSInterface.Instance.Manager.CreateEntityQuery(typeof(AsteroidData)).CalculateEntityCount();
        var remainingUFOs = ECSInterface.Instance.Manager.CreateEntityQuery(typeof(UFOData)).CalculateEntityCount();

        if (remainingAsteroids + remainingUFOs == 0)
        {
            fsm.ChangeState(States.WonLevel);
        }
    }

    void Play_Exit()
    {
        ECSInterface.Instance.Manager.World.GetOrCreateSystem<DestroyDeadPlayerSpaceshipSystem>().SpaceshipDestroyed -= OnSpaceshipDestroyed;
    }

    #endregion PLAY_STATE

    #region WON_LEVEL_STATE

    void WonLevel_Enter()
    {
        Debug.Log($"Won level {CurrentLevel}!");
        // TODO: show some popup or something

        CurrentHyperspaces++;

        fsm.ChangeState(States.Play);
    }

    #endregion WON_LEVEL_STATE

    #region GameOver State
    void GameOver_Enter()
    {
        Debug.Log("Game Over!");
        CurrentLevel = 0;
        fsm.ChangeState(States.Init);

    }
    IEnumerator GameOver_Exit()
    {
        yield return new WaitForSeconds(3f);

    }

    #endregion GameOver State

}
