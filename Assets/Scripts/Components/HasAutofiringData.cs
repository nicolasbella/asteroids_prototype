using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
public struct HasAutofiringData : IComponentData
{
    public float RemainingTime;
    public double lastFiredTime;
}
