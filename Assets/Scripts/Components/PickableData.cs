using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable,  GenerateAuthoringComponent]
public struct PickableData : IComponentData
{
    public enum PickableType
    {
        Forcefield,
        Autofiring,
        Extrafiring

    }
    public PickableType Type;
    
}
