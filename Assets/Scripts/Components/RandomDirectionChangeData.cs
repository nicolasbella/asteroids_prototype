using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable, GenerateAuthoringComponent]
public struct RandomDirectionChangeData : IComponentData
{
    public float MinDelay;
    public float MaxDelay;

    public float MinAngleChange;
    public float MaxAngleChange;

    public float TimeUntilNextChange;
}
