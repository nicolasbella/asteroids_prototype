using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable, GenerateAuthoringComponent]
public struct RotateData : IComponentData
{
    public float3 RotationSpeedEuler;

}
