using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[Serializable]
public struct FXSpawnerData : IComponentData
{
    public EffectType Effect;
    public float3 Position;
    public quaternion Rotation;

    public enum EffectType
    {
        Explosion,
        ExplosionGreen,
        BulletHit,
        Picked,
        HyperSpace
    }
    
}
