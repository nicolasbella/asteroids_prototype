using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Shows player info on UI
/// </summary>
public class UIPlayerInfo : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI scoreLabel;
    [SerializeField]
    TextMeshProUGUI livesLabel;
    [SerializeField]
    TextMeshProUGUI hyperspacesLabel;
    void Start()
    {
        GameManager.TotalScoreChanged += PlayerManager_TotalScoreChanged;
        PlayerManager_TotalScoreChanged();

        GameManager.Instance.CurrentLivesChanged += PlayerManager_LivesChanged;
        PlayerManager_LivesChanged();

        GameManager.Instance.CurrentHyperspacesChanged += PlayerManager_HyperspacesChanged;
        PlayerManager_HyperspacesChanged();
    }

    private void PlayerManager_TotalScoreChanged()
    {
        scoreLabel.text = $"SCORE: {GameManager.TotalScore}";
    }

    private void PlayerManager_LivesChanged()
    {
        livesLabel.text = "\uE82A".Repeat(GameManager.Instance.CurrentLives);
    }

    private void PlayerManager_HyperspacesChanged()
    {
        hyperspacesLabel.text = "hs ".Repeat(GameManager.Instance.CurrentHyperspaces);
    }
}
