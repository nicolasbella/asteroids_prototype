using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Singleton in charge of main UI related parts
/// </summary>
public class UIManager : SingletonMonobehaviour<UIManager>
{
    [SerializeField]
    TextMeshProUGUI startGameLabel;
    [SerializeField]
    TextMeshProUGUI gameOverLabel;
    protected override void Initialize()
    {
        base.Initialize();
        GameManager.Instance.StateChanged += OnGameStateChanged;
        OnGameStateChanged(GameManager.Instance.CurrentState);
    }

    private void OnGameStateChanged(GameManager.States s)
    {
        
        startGameLabel.gameObject.SetActive(s == GameManager.States.Init);

        if (s == GameManager.States.GameOver)
        {
            gameOverLabel.gameObject.SetActive(true);
            gameOverLabel.text = "GAME OVER" + System.Environment.NewLine.Repeat(2) + $"your score: {GameManager.TotalScore}";
        }
        else
        {
            gameOverLabel.gameObject.SetActive(false);
        }

    }
}
