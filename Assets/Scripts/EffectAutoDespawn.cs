using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// handy to automatically despawn FX and control some things like lights, soundsFX and particle systems on it
/// </summary>
public class EffectAutoDespawn : MonoBehaviour
{
    [SerializeField, Header("VFX")] ParticleSystem ps;
    [SerializeField] new Light light;

    [SerializeField] float timeToDespawn = 2f;


    [SerializeField, Header("SFX")] AudioClip clip;
    [SerializeField] float minVol = 1;
    [SerializeField] float maxVol = 1;
    [SerializeField] float minPitch = 1;
    [SerializeField] float maxPitch = 1;

    float currentTimeToDespawn;
    float lightMaxIntensity;

    private void Awake()
    {
        if (light != null) lightMaxIntensity = light.intensity;
        
    }

    private void OnEnable()
    {
        if (ps != null) ps.Play();

        currentTimeToDespawn = timeToDespawn;

        if (clip != null)
        {
            SFXManager.PlaySound(clip, Random.Range(minVol, maxVol), Random.Range(minPitch, maxPitch));
        }
    }

    private void Update()
    {
        currentTimeToDespawn -= Time.deltaTime;

        if (light != null) light.intensity = lightMaxIntensity * (currentTimeToDespawn / timeToDespawn);

        if (currentTimeToDespawn <= 0)
        {
            SimplePool.Despawn(gameObject);
        }
    }

    private void OnDisable()
    {
        if (ps != null) ps.Stop();
    }
}
