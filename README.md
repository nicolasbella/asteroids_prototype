# Asteroids Prototype with new Unity ECS

![picture](https://bitbucket.org/nicolasbella/asteroids_prototype/raw/master/Readme_Images/Title.jpg)

### This game prototype is inspired and follows most of the gameplay designs encountered in the classic 1979 game Asteroids by Atari.

### It's implemented in an hybrid fashion were most of the action happens in the new **Entity-Component-System framework** in Unity.
### When I say *hybrid* I mean that we still use some classic monobehaviour / component's based logic, due to a lack of features in the current ECS framework and to manage some mainthread logic such as the core gameplay loop, VFX, SFX, particle systems, etc.

# Screenshots

![picture](https://bitbucket.org/nicolasbella/asteroids_prototype/raw/master/Readme_Images/Gameplay.jpg)

# Instructions to Run

The Unity version used is **2020.2.7f1** and you should be able to just open the project, the package manager should take care of downloading all dependencies for you.

# Features

most of the original game features are implemented similarly, and then some extra features and content were added upon that. You can check those [here](https://www.gamasutra.com/view/news/294648/Check_out_the_handwritten_game_design_doc_for_Asteroids.php) and [here](https://www.youtube.com/watch?v=WYSupJ5r2zo)

 * **Asteroids spawning** - these spawn at the beginning of each level at the borders of the screen (safest place to avoid unwanted clashing with the player spaceship)
 *  **UFO spawning** - they spawn every now and then, there are two types of UFOs, Big and Small ones
    *  Big UFOs are slower, have more predictable trayectories (the angle of direction change is smaller) and shoot in random directions.
    *  Small UFOs are faster, have more randomized turning directions and shoot aiming at the player with an increasing accuracy over higher levels.
* **Asteroids Lifecycle** - Big asteroids have to be shot 3 times to be destroyed (small difference with the original game mechanic), they divide in 2 medium asteroids that need 2 shots to be destroyed and then 2 small asteroids that endurance a single shot.
* **Pickable Power-ups** - An addition to the original game, power-ups, there are for now two types and they can be combined for an spectacular show of bullets.
    * Auto firing power-up: it allows you to automatically shoot multiple bullets per second by holding Space down.
    * Extra firing power-up: the spaceship shoots 2 extra bullets.
    * Combine them: if you pick both of them you'll have auto shooting for 3 streams of bullets, have fun!
    * Forcefield: an almost invincible forcefield that surrounds the spaceship and gives invulnerability for some time, watch out, it has it's own health so it can be destroyed if you push it through many asteroids (this is to avoid abuse of the forcefield to destroy all asteroids in screen).
* **HyperSpace** - as in the original game, you can use hyperspace (pressing Tab) to inmediately translate the spaceship to a random position in the screen, the main difference here is that on arrival you get a free forcefield with 1 second of duration, that gives you a small time window to escape if you happen to arrive in the middle of an asteroid mess. You get an extra HyperSpace on each level completion.

# Controls

Normal Controls: use **W** to accelerate, **A** and **S** to turn, **Space** to shoot, **Tab** to use Hyperspace.

Debug Controls: use **G** to spawn UFOs, **H** to spawn Asteroids and **P** to spawn Pickables

# Design and Architecture

As I mentioned above, the game is mostly designed around an *ECS pattern*, the UFOs, player spaceship, asteroids, pickables and bullets are all entities with components, there are several systems that alter and supervise their data using jobs and the burst compiler for the highest performance.
I used the GameObject conversion features to simplify prototyping and authoring of the different prefabs in the game, so a ***PrefabsManager*** monobehaviour holds fields with these GameObject prefabs that get converted at runtime into Entity prefabs for later instantiation.
I think this is the best approach for a realistic game development scenario, *Pure ECS* with pure code instantiation and authoring of components data sounds nice on paper, but in reality is easier and faster to deal with the good ol' GameObjects for authoring and even more now that we have Prefab Variants. This is really important since in a development team the artists and game designers need a human-friendly way of authoring and generating content instead of forcing them to learn C#.

I learned a lot about ECS by doing this prototype, and one of the things I learned is that an **EntityCommandBuffer** is not always your best friend, so whenever it made sense, I deferred the sync points to the command buffers, but sometimes a batch instantiation with EntityQueries and the EntityManager is your best option since the ECB does the changes one by one. More on that [here](https://medium.com/@5argon/unity-ecs-batched-operation-on-entitymanager-3c35e8e5ecf4) and [here](https://gametorrahod.com/batched-operation-on-entitymanager/).

The last resource is to use just common mainthread logic to do things, but sometimes you have to because there is no easy way to parallelize, or because you have to do structural changes right at the moment, or because the code needs to communicate with the monobehaviours that run the UI and the VFX/SFX.
you will see that in some places in my code there are scenarios where I just use the EntityManager in main game loop, and sometimes I do workarounds to defer the sync points, for example with the **EffectsSpawnerSystem** and the **FXSpawnerData**.

The Game performance was tested spawning thousands of asteroids and UFOs at the same time, running smoothly at 100+ FPS in an i7 6700K, the main bottleneck is now in the particle systems that spawn for explosions, which are not jobified and the simplePool I used is not having a pool limit so once you start getting hundreds of explosions in screen, both the CPU and the GPU struggle, without VFX we could probably run tens of thousands of entities at 60+ FPS.


# Assets

Most of the assets used were created by me, I'm no expert 3D artist but I managed to create the models, textures and shaders for everything, also some VFX and particle systems.
There are some borrowed sound FX and some made by myself like the spaceship shoot sound, made in Ableton live.

I also used the post-processing stack to add more juice to the final render.

I did use some third party assets to speed up the development and in cases where reinventing the wheel was overengineering. here is the list of 3rd party assets and code used:

 - **[Monster Love's State machine](https://github.com/thefuntastic/Unity3d-Finite-State-Machine)** used to run the core gameplay loop in the GameManager
 - **[SpaceSkies Free](https://assetstore.unity.com/packages/2d/textures-materials/sky/spaceskies-free-80503)** used for the skybox
 - **[Unity Particle Pack](https://assetstore.unity.com/packages/essentials/tutorial-projects/unity-particle-pack-127325)** used for the asteroid explosion
 - **[Quill18's  SimplePool](https://gist.github.com/quill18/5a7cfffae68892621267)** used for the VFX pooling (it is too minimalistic and could be way more performant but I used it for the sake of simplicity)
